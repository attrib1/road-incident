var ctx = document.getElementById("myChart");
var myChart;


function getJsonPolygon(id) {
    var json = null;
   // var url = "http://roadincident.traffy.in.th/api/count_incident_by_date.php?event_begin_since=" + date_since + "%20" + time_since + "&event_begin_till=" + date_till + "%20" + time_till + "&sub_type=" + cat_type;
   var url = 'http://roadincident.traffy.in.th/api/incident_polygon.php?event_begin_since=2013-06-19%2017:21:39&event_begin_till=2013-07-19%2018:41:39&sub_type=all&format=geojson';
   // console.log("" + url);
    $.ajax({
        'async': false,
        'global': false,
        'url': url,
        'dataType': "json",
        'success': function(data) {
            json = data;
        }
    });
    return json;
}

$(document).ready(function() {

    //setChart(countJson);
    chartChang();

});

var randomColorGenerator = function() {
    var color = [];
    // for (var i = 1; i <= 31; i++) {
    //    color.push('#' + (Math.random().toString(16) + '0000000').slice(2, 8));
    // }
    for (var i = 1; i <= 5; i++) {

        color.push('rgba(255, 99, 135, 0.6');
        color.push('rgba(255, 206, 86, 0.6)');
        color.push('rgba(255,0,255,0.6)');
        color.push('rgba(75, 192, 192, 0.6)');
        color.push('rgba(255, 159, 64, 0.6)');
        color.push('rgba(54, 162, 235, 0.6)');
        color.push('rgba(153, 102, 255, 0.6)');

    }
    return color;
};

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year  ;
  return time;
}
function timeConverterMY(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];

  var time =  month + ' ' + year  ;
  return time;
}
function countDate(date,cound_date) {
  var date = new Date(date_since);
    var myNewDate = new Date(date);
    myNewDate.setDate(myNewDate.getDate() + cound_date);
  return myNewDate;

}
function setChart(countJson) {
   // var date_since = document.getElementById("date_since").value;
   // var type_date = document.getElementById("type_date").value;
    var date = new Date(date_since);
    var myNewDate1 = new Date(date);
    //console.log(myNewDate1);
    var countLabels = [476, 915, 874, 501, 546, 956, 973];

    if (type_date == "7") {
        countLabels = [];
         var d =myNewDate1.setDate(myNewDate1.getDate());
        for (var i = 1; i < 8; i++) {
            if(i>=2){
             d =myNewDate1.setDate(myNewDate1.getDate() + 1);
            }
            d = d/1000;
                countLabels.push(timeConverter(d));

        }

    }else if (type_date == "30") {
          countLabels = [];
          //console.log("ddd");
         var d =myNewDate1.setDate(myNewDate1.getDate());
        for (var i = 1; i <= 31; i++) {
            if(i>=2){
             d =myNewDate1.setDate(myNewDate1.getDate() + 1);
            }
            d = d/1000;
                countLabels.push(timeConverter(d));

        }

  } else if (type_date == "185") {
            countLabels = [];
             var d =myNewDate1.setDate(myNewDate1.getDate());
        for (var i = 1; i <= 6; i++) {
            if(i>=2){
             d =myNewDate1.setDate(myNewDate1.getDate() + 31);
            }
            d = d/1000;
                countLabels.push(timeConverterMY(d));
        }
    }
 else {
     countLabels = [];
     var d =myNewDate1.setDate(myNewDate1.getDate());
        for (var i = 1; i <= 12; i++) {
            if(i>=2){
             d =myNewDate1.setDate(myNewDate1.getDate() + 31);
            }
            d = d/1000;
                countLabels.push(timeConverterMY(d));
        }
}

var config = {
    type: 'bar',
    data: {

        labels: countLabels,
        datasets: [{
            label: 'สถิติการเกิดอุบัติเหตุ',
            data: countJson,
            backgroundColor: randomColorGenerator(),
            borderColor: 'rgba(12, 116, 255, 1)',
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
}
myChart = new Chart(ctx, config);
}


Chart.defaults.global.responsive = true;
Chart.defaults.global.hover.mode = 'single';
